<?php

/**
 * @link              https://gitlab.com/DominikKuenkele/polls.git
 * @since             1.0.0
 * @package           Polls
 *
 * @wordpress-plugin
 * Plugin Name:       Plugin Name: Polls

 * Plugin URI:        https://gitlab.com/DominikKuenkele/polls.git
 * Description:       Mit dem Plugin lassen sich Umfragen erstellen und administrieren.
 * Version:           1.0.0
 * Author:            Dominik Künkele
 * Author URI:        https://gitlab.com/DominikKuenkele/
 * Text Domain:       polls
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'POLLS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-polls-activator.php
 */
function activate_polls() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-polls-activator.php';
	Polls_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-polls-deactivator.php
 */
function deactivate_polls() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-polls-deactivator.php';
	Polls_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_polls' );
register_deactivation_hook( __FILE__, 'deactivate_polls' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-polls.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_polls() {

	$plugin = new Polls();
	$plugin->run();

}
run_polls();
