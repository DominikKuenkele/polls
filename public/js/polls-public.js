(function ($) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */


	$(function () {
		/* attach a submit handler to the form */

		$("form#poll-form").submit(function (event) {
			var formHasErrors = false;
			/* stop form from submitting normally */
			event.preventDefault();

			/* get the action attribute from the <form action=""> element */
			var $form = $(this);
			var url = $form.attr('action');

			// check mandatory fields
			$("form#poll-form input.mandatory").each(function() {
				var inputHasErrors = false;
				switch ($(this).attr('type')){
					case 'checkbox':
						if (!$(this).is(':checked'))
							inputHasErrors = true;
						break;
					case 'text':
						if (!$(this).val())
							inputHasErrors = true;
						break;
				}
				if (inputHasErrors) {
					document.getElementById($(this).attr('id')).classList.add('error');
					formHasErrors = true;
				} else {
					document.getElementById($(this).attr('id')).classList.remove('error');
				}
			});

			// check if one answer was selected
			if ($('input[name=poll-answers]:checked').size() == 0) {
				formHasErrors = true;
				document.getElementById('error-message').innerText = 'You must check one answer';
			} else {
				document.getElementById('error-message').innerText = '';
			}

			// send data if checks were succesful
			if (!formHasErrors) {
				/* Send the data using post with element id name and name2*/
				var posting = $.post(url, $form.serialize());

				/* Alerts the results */
				posting.done(function (data) {
					document.getElementById('poll').innerHTML = posting.responseText;
				});
				posting.fail(function () {
					$('#result').text('failed');
				});
			}
		});
	});



})(jQuery);

