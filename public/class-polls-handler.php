<?php

/**
 * The handling of a poll.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Polls
 * @subpackage Polls/public
 */

/**
 * The handling of a poll.
 *
 *
 * @package    Polls
 * @subpackage Polls/public
 * @author     Your Name <email@example.com>
 */
class Polls_Handler {

    private int $id;

    public function __construct (int $id){
        $this->$id = $id;
    }

    public function getQuestion(){
        return 'Wähle einen Jugensprecher';
    }

    public function getAnswers(){
        return [
            1 => 'Person 1',
            2 => 'Person 2',
            3 => 'Person 3'
        ];
    }

    public function getCharacteristics(){
        $characteristics = [
            'name' => [
                'type' => 'text',
                'mandatory' => 'mandatory',
                'text' => 'Name:*'
            ],
            'cookieConsent' => [
                'type' => 'checkbox',
                'mandatory' => 'mandatory',
                'text' => 'Ich willige ein, dass ein Cookie gespeichert wird.*'
            ]
        ];
        return $characteristics;
    }

    public function evaluateSubmission($pollSubmission){
        $hasVoted = false;

        if($pollSubmission['name'] == 'Dominik'){
            $hasVoted = true;
        }
        
        if($hasVoted){
            return 'You have already voted!';
        } else {
            return 'Thanks for the vote!';
        }
    }
}
