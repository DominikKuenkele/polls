<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Polls
 * @subpackage Polls/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Polls
 * @subpackage Polls/public
 * @author     Your Name <email@example.com>
 */
class Polls_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $polls    The ID of this plugin.
	 */
	private $polls;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $polls       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $polls, $version ) {

		$this->polls = $polls;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Polls_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Polls_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->polls, plugin_dir_url( __FILE__ ) . 'css/polls-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Polls_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Polls_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->polls, plugin_dir_url( __FILE__ ) . 'js/polls-public.js', array( 'jquery' ), $this->version, false );

	}

	public function shortcode_poll($atts){
		$attributes = shortcode_atts(array(
			'id' => 0
		), $atts);


		include_once 'class-polls-handler.php';

		$poll_handler = new Polls_Handler($attributes['id']);
		$question = $poll_handler->getQuestion();
		$answers = $poll_handler->getAnswers();
		$characteristics = $poll_handler->getCharacteristics();

		include 'partials/polls-public-display.php';
	}

	public function submit_poll() {
		status_header(200);
		$responseData = $_POST;
		// echo json_encode($responseData);
		// echo json_encode($_SERVER['REMOTE_ADDR']);
		include_once 'class-polls-handler.php';
		$poll_handler = new Polls_Handler(1);
		$question = $poll_handler->getQuestion();

		$message = $poll_handler->evaluateSubmission($_POST);

		include 'partials/polls-public-display-confirmation.php';
	}

}
