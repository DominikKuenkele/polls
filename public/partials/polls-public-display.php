<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Polls
 * @subpackage Polls/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div id="poll">
    <h2><?=$question?></h2>
    <form id="poll-form" name="poll-form" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
    <?php foreach($answers as $answer_id => $answer):?>
        <input id=<?=$answer_id?> type="radio" name="poll-answers" value=<?=$answer_id?>>
        <label for=<?=$answer_id?>><?=$answer?></label>
    <?php endforeach; ?>
    <?php foreach($characteristics as $characteristicId => $characteristicAttributes):?>
        <label for=<?=$characteristicId?>><?=$characteristicAttributes['text']?></label>
        <input class=<?=$characteristicAttributes['mandatory']?> id=<?=$characteristicId?> type=<?=$characteristicAttributes['type']?> name=<?=$characteristicId?>>
    <?php endforeach; ?>
        <input type="hidden" name="action" value="submit_poll">
        <p id='error-message'></p>
        <input type="submit" value="Abstimmen">
    </form>
</div>