<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Polls
 * @subpackage Polls/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<h2><?=$question?></h2>
<p><?=$message?></p>
<p><?=var_dump($responseData)?></p>

